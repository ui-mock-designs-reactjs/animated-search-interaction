import React, { useEffect, useState } from "react";
import "./search.scss";
import { motion } from "framer-motion";
import { CloseOutlined } from "@ant-design/icons";
import { iUser, SAMPLE_USERS } from "../utility/sample-users";
import { UserListItem } from "../components/user-list-item/user-list-item";
import { DebounceInput } from "react-debounce-input";
import { Loader } from "../components/loader/loader";

const variants = {
  open: { height: 600, display: "flex" },
  closed: { height: 0, display: "none" },
};

const overlayVariants = {
  open: { display: "block" },
  closed: { display: "none" },
};

const userVariants = {
  open: { display: "block" },
  closed: { display: "none" },
};

const nameVariants = {
  open: {
    opacity: 1,
  },
  closed: {
    opacity: 0,
  },
};

const avatarVariants = {
  open: {
    height: 50,
    width: 50,
    opacity: 1,
  },
  closed: {
    height: 0,
    width: 0,
    opacity: 0,
  },
};

const searchRows = [
  {
    name: "RECENT SEARCHES",
    list: [
      {
        name: "Emma",
        avatar: "https://randomuser.me/api/portraits/women/4.jpg",
      },
      {
        name: "Albie",
        avatar: "https://randomuser.me/api/portraits/men/5.jpg",
      },
      {
        name: "Jules",
        avatar: "https://randomuser.me/api/portraits/women/11.jpg",
      },
      {
        name: "Gibbie",
        avatar: "https://randomuser.me/api/portraits/men/15.jpg",
      },
      {
        name: "Claiborn",
        avatar: "https://randomuser.me/api/portraits/women/18.jpg",
      },
      {
        name: "Welsh",
        avatar: "https://randomuser.me/api/portraits/women/29.jpg",
      },
      {
        name: "Tommi",
        avatar: "https://randomuser.me/api/portraits/men/30.jpg",
      },
    ],
  },
];

export const SearchExpandable = () => {
  const [searchState, setSearchState] = useState<string>("closed");
  const closeSearchBox = () => {
    setSearchState("closed");
  };

  const openSearchBox = () => {
    setSearchState("open");
  };

  const [users, setUsers] = useState<iUser[]>(SAMPLE_USERS);
  const [search, setSearchText] = useState("");
  const [showLoader, setShowLoader] = useState(false);

  const updateUserList = () => {
    setUsers(
      SAMPLE_USERS.filter(
        (user) =>
          user.first_name.toLowerCase().indexOf(search.toLowerCase()) > -1 ||
          user.last_name.toLowerCase().indexOf(search.toLowerCase()) > -1 ||
          user.email.toLowerCase().indexOf(search.toLowerCase()) > -1
      )
    );
    setShowLoader(false);
  };

  useEffect(() => {
    setTimeout(updateUserList, 800);
  }, [search]);

  return (
    <>
      <motion.div
        animate={searchState}
        variants={overlayVariants}
        onClick={() => closeSearchBox()}
        className="background-overlay"
      ></motion.div>
      <div className="search-expandable">
        <div className="search-text">
          <img
            src="https://img.icons8.com/ios/452/search--v5.png"
            className="search-icon"
          />

          <DebounceInput
            debounceTimeout={300}
            onChange={(e) => {
              setShowLoader(true);
              setSearchText(e.target.value);
            }}
            className="search-field"
            value={search}
            onFocus={() => openSearchBox()}
          />

          {searchState === "open" && (
            <div className="close" onClick={() => closeSearchBox()}>
              <CloseOutlined />
            </div>
          )}
        </div>
        <motion.div
          initial="closed"
          animate={searchState}
          variants={variants}
          transition={{ ease: "easeOut", duration: 0.5 }}
          className="search-filters"
        >
          {searchRows.map((row) => (
            <motion.div className="row">
              <div className="heading">{row.name}</div>
              <div className="content">
                {row.list.map((user) => {
                  return (
                    <motion.div variants={userVariants} className="user">
                      <motion.div
                        transition={{
                          ease: "easeOut",
                          duration: 0.5,
                          delay: searchState === "open" ? 0.7 : 0,
                        }}
                        variants={nameVariants}
                        className="avatar"
                      >
                        <motion.img
                          variants={avatarVariants}
                          transition={{
                            ease: "easeOut",
                            duration: 0.5,
                            delay: searchState === "open" ? 0.3 : 0,
                          }}
                          src={user.avatar}
                        />
                      </motion.div>
                      <motion.div
                        transition={{
                          ease: "easeOut",
                          duration: 0.5,
                          delay: searchState === "open" ? 0.7 : 0,
                        }}
                        variants={nameVariants}
                        className="name"
                      >
                        {user.name}
                      </motion.div>
                    </motion.div>
                  );
                })}
              </div>
            </motion.div>
          ))}

          <motion.div
            variants={nameVariants}
            transition={{ delay: searchState === "open" ? 1.2 : 0 }}
            className="genres"
          >
            <div className="title">ROLE</div>
            <div className="tags">
              <div className="tag">Data Scientist</div>
              <div className="tag">Designer</div>
              <div className="tag">Backend</div>
              <div className="tag">DBA</div>
              <div className="tag">Frontend</div>
              <div className="tag">iOS</div>
            </div>
          </motion.div>

          <motion.div
            variants={nameVariants}
            transition={{ delay: searchState === "open" ? 1.8 : 0 }}
            className="user-list-container"
          >
            <div className="title">
              <span>USERS</span>
              {showLoader && <Loader />}
            </div>
            <div className="user-list">
              {users.map((user) => {
                console.log("User: ", user.id);
                return (
                  <UserListItem
                    first_name={user.first_name}
                    last_name={user.last_name}
                    email={user.email}
                    gender={user.gender}
                    id={user.id}
                  />
                );
              })}
            </div>
          </motion.div>
        </motion.div>
      </div>
    </>
  );
};
