import React from "react";
import "./workout-card.scss";

export const WorkOutCard = () => {
  return (
    <div className="workout-card">
      <div className="date-details">
        <div className="month">AUG</div>
        <div className="date">14</div>
      </div>
      <div className="details">
        <div className="progress">
          <div className="one"></div>
          <div className="two"></div>
        </div>
        <div className="name">Shoulders & Back</div>
        <div className="count">12 Exercises</div>
      </div>
    </div>
  );
};
