import { motion } from "framer-motion";
import React, { useState } from "react";
import "./pop-button.scss";
import newWorkout from "../../new.png";
import { CloseCircleOutlined, FireFilled } from "@ant-design/icons";
import { WorkOutCard } from "../workout-card/workout-card";

const MOBILE_WIDTH = 400;
const BUTTON_HORIZONTAL_SPACE = 25;

const buttonCardVariants = {
  collapsed: {
    backgroundColor: "#fe6c24",
    right: BUTTON_HORIZONTAL_SPACE,
    height: 50,
    width: 50,
  },
  expanded: {
    backgroundColor: "#ffffff",
    left: BUTTON_HORIZONTAL_SPACE,
    right: BUTTON_HORIZONTAL_SPACE,
    top: 40,
    width: MOBILE_WIDTH - BUTTON_HORIZONTAL_SPACE * 2,
    height: 350,
  },
};

const iconVariants = {
  collapsed: {
    opacity: 1,
  },
  expanded: {
    opacity: 0,
    display: "none",
  },
};

interface Props {
  toggleLayover: Function;
}

export const PopButton = (props: Props) => {
  const [popState, setPopState] = useState("collapsed");
  const togglePop = () => {
    setPopState(popState === "collapsed" ? "expanded" : "collapsed");
    props.toggleLayover();
  };
  return (
    <motion.div
      animate={popState}
      initial="collapsed"
      variants={buttonCardVariants}
      transition={{ ease: "anticipate", duration: 0.4 }}
      className="pop-button"
    >
      {popState !== "expanded" && (
        <motion.div
          onClick={togglePop}
          className="icon-container"
          initial={{ opacity: 0 }}
          animate={{
            opacity: 1,
          }}
          transition={{
            delay: 0,
            duration: 0.6,
          }}
        >
          <FireFilled />
        </motion.div>
      )}

      {popState === "expanded" && (
        <motion.div
          transition={{
            delay: 0.5,
            duration: 0.3,
          }}
          initial={{ display: "none", opacity: 0 }}
          animate={{ display: "flex", opacity: 1 }}
          className="card-content"
        >
          <div className="workout-header">
            <div className="left">
              <img src={newWorkout} style={{ width: 60 }} />
              New Workout
            </div>
            <div
              className="right"
              onClick={togglePop}
              style={{ fontSize: 24, color: "gray" }}
            >
              <CloseCircleOutlined />
            </div>
          </div>
          
          <div className="cards-heading">From previous</div>
          <div className="scroll">
            <div className="cards">
              {[1, 2, 3, 4, 5].map((num) => (
                <div className="shadow-container">
                  <WorkOutCard />
                </div>
              ))}
              <div style={{ width: 20 }}>&nbsp;&nbsp;&nbsp;</div>
            </div>
          </div>

          <div className="start-button">Start</div>
        </motion.div>
      )}
    </motion.div>
  );
};
