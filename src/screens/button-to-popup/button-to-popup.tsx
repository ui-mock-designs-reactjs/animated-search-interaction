import {
  CaretRightOutlined,
  CloseCircleFilled,
  CloseCircleOutlined,
  CloseOutlined,
  FireFilled,
  FireOutlined,
} from "@ant-design/icons";
import { motion } from "framer-motion";
import React, { useState } from "react";
import "./button-to-popup.scss";
import { PopButton } from "./components/pop-button/pop-button";
import { WorkOutCard } from "./components/workout-card/workout-card";
import newWorkout from "./new.png";

const MOBILE_WIDTH = 400;

export const ButtonToPopup = () => {
  const [showLayover, setShowLayover] = useState(false);
  const toggleLayover = () => setShowLayover(!showLayover);
  return (
    <div className="button-to-popup">
      <div className="mobile-screen" style={{ width: MOBILE_WIDTH }}>
        {showLayover && <div className="layover"></div>}

        <PopButton toggleLayover={toggleLayover} />
        <div className="mobile-header">
          <img
            className="avatar"
            src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/female-runner-choosing-smartphone-music-on-high-res-stock-photography-640355753-1548180597.jpg"
          />
          <div className="welcome">Hello Jules</div>
        </div>
        <div className="heading-tabs">
          <div className="heading-tab active">Stats</div>
          <div className="heading-tab">Muscles</div>
        </div>

        <div className="workouts">
          <div className="count">58</div>
          <div className="text">Workouts Completed</div>
        </div>

        <div className="weight-goals">
          <div className="current">
            <div className="weight">
              <div className="number">72.4</div>
              <div className="unit">kg</div>
            </div>
            <div className="text">Current Weight</div>
          </div>
          <div className="left-to-gain">
            <div className="top">
              <div className="weight">
                <div className="number">7.6</div>
                <div className="unit">kg</div>
              </div>
            </div>
            <div className="middle">
              <div className="left"></div>
              <div className="right"></div>
            </div>
            <div className="bottom">
              <div className="text">Left to Gain</div>
            </div>
          </div>
        </div>

        <div className="stats">
          <div className="stat">
            <div className="left">
              <div className="top">
                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEA8SDg8PDxAQEA8VDw8QDw8PDg8QFRUWFhUSFRUYHSggGBomHRUVITEhJSktLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGi0mICUtLS0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0rLS0tLS4tKy0tLSstLy0tLS0tLf/AABEIAPkAygMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQIDBAUGB//EAD8QAAIBAgIHBQYEBAQHAAAAAAABAgMRBBIFITFBUWFxBiKBkbETMlKhwdEjYnLhB0KC8HOSsvEUFSQzU6LC/8QAGwEAAgMBAQEAAAAAAAAAAAAAAAQCAwUGAQf/xAA5EQABAwIDBQcDAwEJAQAAAAABAAIDBBEhMUEFElFhcRMikaGxwfAygdEzQuEjFBUkNFKistLxBv/aAAwDAQACEQMRAD8A9xAABCAAAQgAAEIAABCAAAQgAAEIAirVYwi5TkoxiruUmlFLi2ziO0H8RaVO8MHD289a9pK8aMXy3z+S5kHyNbmr4KaWd1oxf0HVdzUqKKbk1FLa20kvE5bS3bzBULqM3iJcKLjKN+c27eVzy7S2ncVi3evWqSW6MXlprpFavqZqiKvqj+0Ldg2IwYzOvyGA8c/RdppL+JWKndUIU8PHdK3tJ+b1fIsdhP8Aicfiva4qtVq0sPaWWUn7OVV3yLKtWrW/BHB5T2j+H+jlQwNLV3qy9rL+pd3/ANcpCImR/eKsr2w0lOezaATgDrzxOPnqunAANBcugAAEIAABCAAAQgAAEIAABCAAAQgAAEIAABCDB7S9p8Pgo9956rV4UYtZnzfwrmZHbHtksPmo4ZqdfZOWpwo/eXLdv4HmFepKcpTnJyk23KUneUnxbFJ6kN7rc1s0GyjKBJLg3Qan8DzV/T/aLE4yV6s8tNPu0o3VOPh/M+bMdQJ1EFEzy8k3K6VjGsaGtFhwCiUBVEmVN2vZ2421DlAjvKSbh6GecIr+acY+bS+p7/RoqEYxjqUYxiuiVkeJ9nad8XhU/wDz0P8AXE9xH6HHeK57brsY29UAADywEAAAhAAAIQAACEAAAhAAAIQAACEAAAhBxXbftV7BOhh5fjNd+af/AGk9y/M/kana7T6wlHu2dapdU1tyrfUfJep5JNuTbk3KTbbk3dtva2xGrqdzuNz1W1sqgEn9aQd3QcT+B5npjDZ79+97WCgTqBfwuAW2f+X7mS+UNFyulc62JVHD4OU9isuL2GlQ0fCO3vPns8i2kAm+ZzuSpLyVm6UeuMVsSuUspdxyvN9EQqAxGbNAVjMlNoeWXEUJfDWpPymj288NjG2zce0YGt7SlSn8dOnL/MkzV2c++83oVhbcYe47qPT+VaAANNc+gAAEIAABCAAAQgAAEIAABCAAAQgr4vExpU51Ju0IRcpPkiwcP/EPSeqGHg9tp1en8sfV+CKZ5REwu+XTFJTmolEY+/Ia/OK43TWkJ4qvOpP+Z2jH4YL3Y/3zKagSKBcwtC3efh9znXy27xXa2axoAyGQRhcKo6373oWQHQi27JXb2JCbnEm5VJOpSCF+eEVOm5T1yepLcm/VlEra8OyUGuDslTxce91RBYuYqOpPgVbDbHd1MsOCbY9L7CYz2mEUW9dKTj/S+9H1t4Hm1jp+wWN9niJU29VWNlwzxu18sw5RS7kwvrgkdqRdrTG2Yx8M/K69GAAOhXIIAABCAAAQgAAEIAABCAAAQgAAEKOrUUYylJ2UU23wSV2eP6Txbr1qlWW2UpPpHYl5WPRe2eM9nhJpbajVNdHrl8k/M8ysY20pe8GDTHx+ea6PYkNmOlOuH2H8+idQp3fJbS6Mowsub2khiudcrWcblLCDk0krt7EbmDwiprjJ7X9ER6NwuRZn70vkuBdM2om3jujL1SE0u8d0ZeqztNS1QXO/9+Zkmpppe5/V9DLGaf8ATCvg+gJJK6sVHGxcIa8N/mMsOiYYbYKvYmw9WVOcJx1OElKPVO4ywFl1Mr2DB4mNWnCpH3ZxUl4osHJ9hMdmpToyeuk7x/RLd4O/mdYdTBKJYw/j8K4qphMMro+B8tPJAABcqEAAAhAAAIQAACEAAAhVcdi4Uac6lR2jFXfFvclzZHofFOtQp1JKzqJystybdl5WOF7XaZ9vUyU3+FSb2bJz3y6bl+51PYvEqeEgt9KUoPzuvk0IxVYkqCxuQB+51WlNQmGlEjvqJH2Fjh14rF/iLiddGnwjKb8XlXozkaMLs6Ht3O+Kt8NOmvV/Uw6C1XMaufeZ5+y3aFu5SsHL1xUpb0dQzz17I639EVDb0XSy0098tfhuMud+4w2+fApTO3Wq4AgplpFUdLQvBP4ZLyer7GMdJVgpRcXvTRz04NNp7U7MfpXd0t4e6bp3d2yYDQtgsNphVpRsFiarDVfgRWJtNwph1wtPszjfYYmDbtGbyT/TLf4Oz8D1E8cseoaAxnt8PSm9uXLP9cdT9L+JtbKm+qM9R6H2WHtmH6ZR0PqPfyWmAAbCwkAAAhAAAIQAACEHMdtNL+xpeyg/xKqd7bY09jfV7PM6HEV404SnN2jFNyfJHlWk8ZLEVZ1Z7W9S+GO6PkZ+0Kjs490Zu9NT7LT2XS9rLvuGDfM6flU7G92Q0qsPWyzdqVWyk3sjJe7J+niYVhbGFHIY3BzcwukljbKwsdkVudtYf9XN8Y02v8qX0MqCskJVrSm455OWWKjFvW8q2L5kmUjUSB8hcNTdVxtMcbWHQW8E1I6WnCyS4JIwqELyj+pep0RlVbsh1S9S7JMsLYWwondK3SGZpXDa1Nb9Uuu5mpYbOmpJp7GicUhY66kx+6665zKLYsVqLhJp7tnNcSOxp718U+HXTMhWqQs/QvWI61O65o9a+xXrXWKpWOy7A4vVWpN7LTivlL/5OQsanZjE+zxVHhOWR9JavWw9Ry9nO087eOH8qqsj7SBzeV/uMf4XpZWxuMp0YOdWShHntb4Jb2OxmJjShKc3aMFdnmmldIzxNRzm9X8kL92MeC+5vVtYKcWGLjkPc+3Fc/RUZqDc4NGf4HvwXeaF0usS6rhBxhBxUXJ96Td27rduNY5TsHJezrLepwfg1b6HVllHI6SFr3Zn8lVVkbY5nMaMBb0CAABlLIAABC5Pt3pHLCFGL11O9P8AQnqXi18jh7GjpzGe3xFWpucmofojqX38SlGN3Y5arn7WUu0yHQfLrr6OHsYWt1zPU/LfZLRp7/IZYuqNitVhZibXXKva65TKUdaLmUgoLvItqJB5xUXnFLh134fqXqb1jDpqzT4NG7Yz6rMJOozCLDqcLuwhaw8LK+9+gsMSlHu3QoK671luSI7Etf3mMPCcUN+kKpjcPnV17y2c1wMvKb1jPx2Hs8y2PbyYxBLbulMxP/aqNhbD8othm6vus7EU7Pk9aGwk001tTTXVay7i6d434ehTsMMdvNVzHXC6ztlj81GhGL1VUqkucbLL838jkbGjpXEe0WG/JQhHylJFCw7WTdrKXdPQe91TSxCGMMHP1Wz2RxypYhKTtGqsj5Sv3X56vE9CPJrHedmdLe3p5Zv8Wmlf80d0vuaGyqofou6j3Hv4rN2rT3/rN6H2Psfst0AA3FiIMztBifZYatJbcrjHrLur1NM5ft3XtRpw+Obb6RX3kherk7OFzhw8zgPNMUkYknY08fIYnyXDJE+Fhrb4EVi7hI93qzkHmzV1r3YJ2UbVpZlz3E1h1hfesqLlZ9CNpJM1sFQu5N7En5tEEqSbT3rea2FpZYJb3rfiVVMvd5queXurIsbVN3SfFIy5Qs2uDZo4N3guWopqDcA/MVXPiAVYpwu0i8V8NHaycpZldZ0jsVVxC73gR2LGIjqTIMpW/NWsN2pLCTgmmnsY+wtiKksirSytpjbGjiqV1fevQpWHGSbwummP3go3G5mThZtcGbFjPxsLS6oYhfjZXRuxsq1ixh8M5a3qXqLhqGZ8lt+xpKFthOWbdwClJJbALLxUUpWWpJIXB4iVKcakHaUX4Nb0+Q7Ervy6kVixji0AjPBSFi2xXpWAxca1ONSGyS2b096ZZOS7G4uznSb1NZ49VZNeVvI607KjqO3hD9deo+X6FcvVQ9jKWaadEHFdu3epQXCDfm/2O1OI7cr8an/hL/Uyjah/wx6j1TGy/wDMDofRczY0MPHux6FKxp0Y92PRHJzmwC6GU4JMo7KPyi2Ft5UXS4eneSXn0NSxVwMNr8C7YTmdd1uCVlddyy8VTtJ89ZYwGyS6Mmr0cy4NbGNw1BxvfeemQGO2q9LwWWV+itSHiQWpdBbE2g2SBOKSSuVmrFuwyrTvrRGSMkXCkx1iq4WHCWFVekKNalZ8nsNAixELroTjfYqTHWKo5SrpCGqL5tF/KR16d0l+dDbH2cCmGusbqPDUssUt+19SfKOsLYgX3NyolyycXHvy8PQiylvGx776IhyjrHd0JlrsArWgpZcRRf57PpJW+p6Aef6NX4tL/Eh6o9AOn2E7eieOfqB+FjbU+tp5ION7dU/xKMuMJLya+52KOe7Z4fNRhL4J6+ktXqkObSbvUzuVj4EFUbPfu1Dedx4iy4mxq0F3Y9EZtjUwq7kTjJzgF0MpwTrC5R+UdYV3lRdWcLG0VzJghGyXRC2EnOuSUoXXN0BYWwWIkry6twjqXQdlHUleKH5TfjgBAKTJxUeUXKSZRcow2nUd5Uq9G2tbN/IhsauUpYmhl1rZ6GZX0BYO1YMNeXPpx4Z9Lo5b4FVwaHBYy7K1U3ETKS1I62NsX7yv3k2wth1hbHm8i6zsdHvLoQKJbxi7y6EKiNsd3QmWHuhT6Ip3r0l+dPy1/Q7k5fs3hr1HPdBfN/tc6g7DYTC2nLj+4m3QAD1BWPtB+9KBwC5v/mTwtedKrd0W81OWtunGWu3ON7rwNTSGSrhqtpKUXTk1JO6uldfNGZ2uwmaEKqWuDtL9L2Pz9Tl4yaTSbSe1JtJ9UU1Fa+lkdA8bzTe2OIBvhzF8B0TMVO2oa2Vps4Z9Rr11+6iSNDAe70ZUylrR+1rivQ5qU9xacpu1W8oqiPsOSE7pXeVmwCi2FAEtdNFaJ8LRzPXsW0lx1LWnx2jjaN5pzPoD5anpe3nwVZeN7dS4PXHoWMpVwLtK3FmhY6HZlpKdvLDwy8rJeXByiUR2UdYdY0mwqreUeUHBNWewksLYsEQXl1k16OV23bmR2NbEUc0bb9xl2OR2lQ/2aWw+k4jlxH2PkU3HJvBV6q1iElVaxpm3smAcE2wth1gI7yLrPxKvJ+AyMG2kldvYuJK1d9Td0RozJac13v5Y/DzfM16GkkqpBGzIZnQD88Br0uRZLMImXKt6MwipU1Hftk+bLpnYzH2kqdO0qj1co/uWY4dWV3K+/Wzt4JWC8UAuGWBxFhyucyNeGuax3tce+/XH57J1eipwlCWySaficDicO6c5Qlti2uvBnohz/aXAXSqxWuPv81ufgJ7Ype0i7RubfTXwz8U3s+fcfuHI+vzBcxlJaDtJPmJlFynIl11sk6LTsLYbh5XivmSiBNsEmTY4qdIWwsNiFSIWS91o4OnaKJKlNSTT3kkY2SHWPoUdK1kQitgBbysfHVZ5fc3WNKDhLXtRqwd0mt6Fq0VLavEWnTypJbhSg2c+lleAbsOI4g/+a64c1N8gcBxRYdYWwtjV3FVdJYLDrBYnuLy6bYzcZDLN89fkatihpOPuvqjL21AHUhd/pIPnb3VkLrOWZJawyj7C2OG3CSn7pmUixGy3EsEVKqlNSazKOxbOhNkYLgHGwvnw58cENON+Cv6O0eqaz1LZtv6Ob5kWP0m3eNLUt8v72FfFYqdT3nZbo/3tDB4Z1JW3La+Buv2gXAUlA0gHC+TnHU8QNSc7XyAsoCPHtJcT5BW9C4XbUl/T9TYGQikrJWS2IedXQ0jaWBsTdMzxOp/HKyRlkMji4oGTgmmmrpqzXFDwG1WuN0lgHRm/heuD5fdFbKdljMNGrFxl4Pejl8ThZU5OMlr3Pc0cTtXZ5pX77B3DlyPD8eGa2qaq7QWOfrz/ACmYR2duPqXCkol2jK657zAlGoU5eKmpEkdq6kcUPJxmwvwSzs1uNCWCnK8U+I6x9MADsRqs1JYLDrCkt1RTQHAe2QksFhQPbIRYoaU2R6svmbpSXeguH1/2M3bLt2ifz3R/uCshHfCoiiAcMnkypLcMUSXKSUaDk7RX7FIjkleA0G5yClvBoUNGg5NKK1v5G7hqKpxsv9xMPh1Batu98Sc7jZGyRSN334vPkOA9z9hhmlNMX4DJAABtqhAAAIQVsXhI1Y2l4PgyyBCSNsjSx4uDmCvQSDcLlsVg5U3aS1bpLeR0207o6qcFJWkk09xmYnRW+m/6f3OQr9gyx3fT94cP3D/t/wAuRzWhHVh2D/4VSEk9gpFKnKD1rK+hJGVzADrEtIsRopkahamjqt4uO+JdMKjVcJJrd80bNKopK6O22LWtlhETvqbh1Gh+2R6XyKRmZY30KkAANtUoAABCAAAQgxMTUzTk917LwL+Nr5VZe8zLOV/+grASIGnLF3XQeBueoTMDP3IAno4Sct1lxL1HBxjrfefH9jOpdlVFRjbdHE4eAzPpzVjpWtVPD4SU9b1R47bmnTpqKslYkA6ui2bDSDu4u1cc/wCB0z1JSz5C7NAABoKtAAAIQAACEAAAhAAAITJxT1NXRWngIPYrdC4BRPSwz/qsDuoFx0OYUmuLcis2ejXulfqrDaeHqwd4rrZrWagGf/clMHB0Zc0jKzvzdT7Z2RxVaniN004v5eZOnw1jiOnvNFrZI7B7t6+trH72wP2AVeByUgABcvEEdTNbu2vx4EgEXNuLXt0+fNEKisAm7zlmb27ixSoRjsjYmAWhoaeE7zGAHjmfE3PmpF7jmUAADaigAAEIAABCAAAQv//Z" />
              </div>
              <div className="middle">
                <div className="number">12.6k</div>
                <div className="unit">cal</div>
              </div>
              <div className="bottom">Burned</div>
            </div>
            <div className="middle"></div>
            <div className="right"></div>
          </div>
          <div className="stat">
            <div className="left">
              <div className="top two">
                <img
                  // style={{ opacity: 0.7 }}
                  src="https://icons-for-free.com/iconfiles/png/512/dumbbell-1324760573293036990.png"
                />
              </div>
              <div className="middle">
                <div className="number">270k</div>
                <div className="unit">kg</div>
              </div>
              <div className="bottom">Lifted</div>
            </div>
            <div className="middle"></div>
            <div className="right"></div>
          </div>
          <div className="stat">
            <div className="left">
              <div className="top three">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRetsktPedoqXm9BnX0NjQRFtyclI22VNrKM3wnfw141el9EuMhf-FrrAO2BCnFS5U3-7E&usqp=CAU" />
              </div>
              <div className="middle">
                <div className="number">13</div>
                <div className="unit">weeks</div>
              </div>
              <div className="bottom">Training</div>
            </div>
            <div className="middle"></div>
            <div className="right"></div>
          </div>
        </div>

        <div className="bottom-details">
          <WorkOutCard />
          <div className="right-button">
            <CaretRightOutlined />
          </div>
        </div>
      </div>
    </div>
  );
};
