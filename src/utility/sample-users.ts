export interface iUser {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
}

export const SAMPLE_USERS: iUser[] = [
  {
    id: 1,
    first_name: "Lucy",
    last_name: "Goldstein",
    email: "lgoldstein0@ning.com",
    gender: "Female",
  },
  {
    id: 2,
    first_name: "Chic",
    last_name: "Hartle",
    email: "chartle1@arizona.edu",
    gender: "Female",
  },
  {
    id: 3,
    first_name: "Melony",
    last_name: "Woodhouse",
    email: "mwoodhouse2@typepad.com",
    gender: "Female",
  },
  {
    id: 4,
    first_name: "Dannye",
    last_name: "Sobey",
    email: "dsobey3@ucla.edu",
    gender: "Female",
  },
  {
    id: 5,
    first_name: "Albie",
    last_name: "Jacmard",
    email: "ajacmard4@quantcast.com",
    gender: "Male",
  },
  {
    id: 6,
    first_name: "Benjamen",
    last_name: "Orae",
    email: "borae5@umich.edu",
    gender: "Female",
  },
  {
    id: 7,
    first_name: "Arielle",
    last_name: "Christopher",
    email: "achristopher6@sfgate.com",
    gender: "Female",
  },
  {
    id: 8,
    first_name: "Cosme",
    last_name: "Slyvester",
    email: "cslyvester7@wsj.com",
    gender: "Female",
  },
  {
    id: 9,
    first_name: "Bea",
    last_name: "Barhems",
    email: "bbarhems8@answers.com",
    gender: "Female",
  },
  {
    id: 10,
    first_name: "Rudd",
    last_name: "Ingyon",
    email: "ringyon9@adobe.com",
    gender: "Male",
  },
  {
    id: 11,
    first_name: "Creight",
    last_name: "Sandall",
    email: "csandalla@ed.gov",
    gender: "Female",
  },
  {
    id: 12,
    first_name: "Barnaby",
    last_name: "Woolatt",
    email: "bwoolattb@netlog.com",
    gender: "Male",
  },
  {
    id: 13,
    first_name: "Kingsley",
    last_name: "Corder",
    email: "kcorderc@booking.com",
    gender: "Male",
  },
  {
    id: 14,
    first_name: "Stephannie",
    last_name: "Meeland",
    email: "smeelandd@netscape.com",
    gender: "Female",
  },
  {
    id: 15,
    first_name: "Gibbie",
    last_name: "Pashenkov",
    email: "gpashenkove@freewebs.com",
    gender: "Male",
  },
  {
    id: 16,
    first_name: "Laurel",
    last_name: "Masterson",
    email: "lmastersonf@si.edu",
    gender: "Male",
  },
  {
    id: 17,
    first_name: "Rosie",
    last_name: "Adanez",
    email: "radanezg@mac.com",
    gender: "Male",
  },
  {
    id: 18,
    first_name: "Claiborn",
    last_name: "Scola",
    email: "cscolah@google.ca",
    gender: "Female",
  },
  {
    id: 19,
    first_name: "Salvatore",
    last_name: "De Bruin",
    email: "sdebruini@blog.com",
    gender: "Female",
  },
  {
    id: 20,
    first_name: "Karoline",
    last_name: "Woodington",
    email: "kwoodingtonj@marketwatch.com",
    gender: "Male",
  },
  {
    id: 21,
    first_name: "Sophey",
    last_name: "Thirtle",
    email: "sthirtlek@archive.org",
    gender: "Male",
  },
  {
    id: 22,
    first_name: "Retha",
    last_name: "Stolting",
    email: "rstoltingl@google.pl",
    gender: "Male",
  },
  {
    id: 23,
    first_name: "Opal",
    last_name: "Tumasian",
    email: "otumasianm@walmart.com",
    gender: "Female",
  },
  {
    id: 24,
    first_name: "Norrie",
    last_name: "Erickssen",
    email: "nerickssenn@hostgator.com",
    gender: "Male",
  },
  {
    id: 25,
    first_name: "Nertie",
    last_name: "Shopcott",
    email: "nshopcotto@people.com.cn",
    gender: "Female",
  },
  {
    id: 26,
    first_name: "Becka",
    last_name: "Brittan",
    email: "bbrittanp@psu.edu",
    gender: "Male",
  },
  {
    id: 27,
    first_name: "Lemuel",
    last_name: "Gardiner",
    email: "lgardinerq@cornell.edu",
    gender: "Male",
  },
  {
    id: 28,
    first_name: "Hussein",
    last_name: "Blaskett",
    email: "hblaskettr@photobucket.com",
    gender: "Female",
  },
  {
    id: 29,
    first_name: "Welsh",
    last_name: "Farrant",
    email: "wfarrants@artisteer.com",
    gender: "Female",
  },
  {
    id: 30,
    first_name: "Tommi",
    last_name: "Maddyson",
    email: "tmaddysont@51.la",
    gender: "Male",
  },
  {
    id: 31,
    first_name: "Lanny",
    last_name: "Georgeon",
    email: "lgeorgeonu@tinyurl.com",
    gender: "Male",
  },
  {
    id: 32,
    first_name: "Sean",
    last_name: "Davidson",
    email: "sdavidsonv@sfgate.com",
    gender: "Male",
  },
  {
    id: 33,
    first_name: "Tommie",
    last_name: "Nott",
    email: "tnottw@shop-pro.jp",
    gender: "Female",
  },
  {
    id: 34,
    first_name: "Cleveland",
    last_name: "Trodler",
    email: "ctrodlerx@wp.com",
    gender: "Male",
  },
  {
    id: 35,
    first_name: "Ashleigh",
    last_name: "Doody",
    email: "adoodyy@geocities.com",
    gender: "Female",
  },
  {
    id: 36,
    first_name: "Kristoforo",
    last_name: "Petrazzi",
    email: "kpetrazziz@upenn.edu",
    gender: "Male",
  },
  {
    id: 37,
    first_name: "Katee",
    last_name: "Iffe",
    email: "kiffe10@blinklist.com",
    gender: "Female",
  },
  {
    id: 38,
    first_name: "Alden",
    last_name: "Broek",
    email: "abroek11@devhub.com",
    gender: "Male",
  },
  {
    id: 39,
    first_name: "Saleem",
    last_name: "Yeude",
    email: "syeude12@ifeng.com",
    gender: "Female",
  },
  {
    id: 40,
    first_name: "Brana",
    last_name: "Glyde",
    email: "bglyde13@bloglines.com",
    gender: "Male",
  },
  {
    id: 41,
    first_name: "Thoma",
    last_name: "Firmage",
    email: "tfirmage14@nasa.gov",
    gender: "Male",
  },
  {
    id: 42,
    first_name: "Alyse",
    last_name: "Mayne",
    email: "amayne15@va.gov",
    gender: "Male",
  },
  {
    id: 43,
    first_name: "Aube",
    last_name: "Buttrey",
    email: "abuttrey16@chron.com",
    gender: "Male",
  },
  {
    id: 44,
    first_name: "Leola",
    last_name: "Lesslie",
    email: "llesslie17@bigcartel.com",
    gender: "Female",
  },
  {
    id: 45,
    first_name: "Freddy",
    last_name: "Halpen",
    email: "fhalpen18@youku.com",
    gender: "Male",
  },
  {
    id: 46,
    first_name: "Joellyn",
    last_name: "Harrowing",
    email: "jharrowing19@cnet.com",
    gender: "Male",
  },
  {
    id: 47,
    first_name: "Clemmy",
    last_name: "Heyns",
    email: "cheyns1a@state.gov",
    gender: "Male",
  },
  {
    id: 48,
    first_name: "Findlay",
    last_name: "Faley",
    email: "ffaley1b@merriam-webster.com",
    gender: "Male",
  },
  {
    id: 49,
    first_name: "Nick",
    last_name: "Drexel",
    email: "ndrexel1c@bravesites.com",
    gender: "Male",
  },
  {
    id: 50,
    first_name: "Shina",
    last_name: "Michelotti",
    email: "smichelotti1d@dyndns.org",
    gender: "Female",
  },
  {
    id: 51,
    first_name: "Elwira",
    last_name: "Byas",
    email: "ebyas1e@craigslist.org",
    gender: "Male",
  },
  {
    id: 52,
    first_name: "Penrod",
    last_name: "Sharrem",
    email: "psharrem1f@java.com",
    gender: "Male",
  },
  {
    id: 53,
    first_name: "Winn",
    last_name: "Ellingford",
    email: "wellingford1g@nytimes.com",
    gender: "Male",
  },
  {
    id: 54,
    first_name: "Heath",
    last_name: "Askin",
    email: "haskin1h@shareasale.com",
    gender: "Female",
  },
  {
    id: 55,
    first_name: "Lana",
    last_name: "Steptow",
    email: "lsteptow1i@howstuffworks.com",
    gender: "Female",
  },
  {
    id: 56,
    first_name: "Tallia",
    last_name: "Ramel",
    email: "tramel1j@163.com",
    gender: "Male",
  },
  {
    id: 57,
    first_name: "Christina",
    last_name: "Rowbury",
    email: "crowbury1k@comsenz.com",
    gender: "Male",
  },
  {
    id: 58,
    first_name: "Sheba",
    last_name: "Brotherton",
    email: "sbrotherton1l@google.com.au",
    gender: "Male",
  },
  {
    id: 59,
    first_name: "Paxton",
    last_name: "Geaney",
    email: "pgeaney1m@google.nl",
    gender: "Female",
  },
  {
    id: 60,
    first_name: "Kalvin",
    last_name: "Adshede",
    email: "kadshede1n@berkeley.edu",
    gender: "Female",
  },
  {
    id: 61,
    first_name: "Noami",
    last_name: "Fortesquieu",
    email: "nfortesquieu1o@mozilla.org",
    gender: "Female",
  },
  {
    id: 62,
    first_name: "Ayn",
    last_name: "Malcolmson",
    email: "amalcolmson1p@dropbox.com",
    gender: "Male",
  },
  {
    id: 63,
    first_name: "Adolpho",
    last_name: "Bisco",
    email: "abisco1q@ucoz.com",
    gender: "Female",
  },
  {
    id: 64,
    first_name: "Gabrila",
    last_name: "Spacy",
    email: "gspacy1r@cocolog-nifty.com",
    gender: "Male",
  },
  {
    id: 65,
    first_name: "Wait",
    last_name: "Baudy",
    email: "wbaudy1s@gizmodo.com",
    gender: "Female",
  },
  {
    id: 66,
    first_name: "Robina",
    last_name: "Barkaway",
    email: "rbarkaway1t@sina.com.cn",
    gender: "Male",
  },
  {
    id: 67,
    first_name: "Gary",
    last_name: "Fowley",
    email: "gfowley1u@tiny.cc",
    gender: "Female",
  },
  {
    id: 68,
    first_name: "Livvy",
    last_name: "Phelan",
    email: "lphelan1v@4shared.com",
    gender: "Female",
  },
  {
    id: 69,
    first_name: "Felecia",
    last_name: "Wiggam",
    email: "fwiggam1w@phoca.cz",
    gender: "Male",
  },
  {
    id: 70,
    first_name: "Delainey",
    last_name: "Blune",
    email: "dblune1x@telegraph.co.uk",
    gender: "Female",
  },
  {
    id: 71,
    first_name: "Adorne",
    last_name: "Danilchik",
    email: "adanilchik1y@cdc.gov",
    gender: "Female",
  },
  {
    id: 72,
    first_name: "Fionnula",
    last_name: "Starzaker",
    email: "fstarzaker1z@phpbb.com",
    gender: "Female",
  },
  {
    id: 73,
    first_name: "Aarika",
    last_name: "Temby",
    email: "atemby20@wikia.com",
    gender: "Female",
  },
  {
    id: 74,
    first_name: "Chanda",
    last_name: "Astridge",
    email: "castridge21@nhs.uk",
    gender: "Female",
  },
  {
    id: 75,
    first_name: "Francis",
    last_name: "Brezlaw",
    email: "fbrezlaw22@hostgator.com",
    gender: "Female",
  },
  {
    id: 76,
    first_name: "Yetty",
    last_name: "Cowlishaw",
    email: "ycowlishaw23@indiatimes.com",
    gender: "Male",
  },
  {
    id: 77,
    first_name: "Celia",
    last_name: "Stothart",
    email: "cstothart24@wsj.com",
    gender: "Female",
  },
  {
    id: 78,
    first_name: "Cullin",
    last_name: "Benninger",
    email: "cbenninger25@yahoo.co.jp",
    gender: "Female",
  },
  {
    id: 79,
    first_name: "Caryl",
    last_name: "Casarini",
    email: "ccasarini26@yahoo.com",
    gender: "Female",
  },
  {
    id: 80,
    first_name: "Gwenneth",
    last_name: "Weildish",
    email: "gweildish27@360.cn",
    gender: "Male",
  },
  {
    id: 81,
    first_name: "Aldo",
    last_name: "Skyram",
    email: "askyram28@yale.edu",
    gender: "Male",
  },
  {
    id: 82,
    first_name: "Forrester",
    last_name: "Hammatt",
    email: "fhammatt29@google.pl",
    gender: "Male",
  },
  {
    id: 83,
    first_name: "Izabel",
    last_name: "Pochet",
    email: "ipochet2a@desdev.cn",
    gender: "Female",
  },
  {
    id: 84,
    first_name: "Melvin",
    last_name: "Creaney",
    email: "mcreaney2b@skyrock.com",
    gender: "Female",
  },
  {
    id: 85,
    first_name: "Fanni",
    last_name: "Haglinton",
    email: "fhaglinton2c@163.com",
    gender: "Male",
  },
  {
    id: 86,
    first_name: "Cheryl",
    last_name: "Springford",
    email: "cspringford2d@odnoklassniki.ru",
    gender: "Male",
  },
  {
    id: 87,
    first_name: "Bradney",
    last_name: "Alexsandrovich",
    email: "balexsandrovich2e@google.cn",
    gender: "Male",
  },
  {
    id: 88,
    first_name: "Sisely",
    last_name: "Daviddi",
    email: "sdaviddi2f@google.ru",
    gender: "Male",
  },
  {
    id: 89,
    first_name: "Julietta",
    last_name: "Spuffard",
    email: "jspuffard2g@clickbank.net",
    gender: "Male",
  },
  {
    id: 90,
    first_name: "Devin",
    last_name: "Harler",
    email: "dharler2h@businessinsider.com",
    gender: "Female",
  },
  {
    id: 91,
    first_name: "Amandy",
    last_name: "Bythell",
    email: "abythell2i@spotify.com",
    gender: "Male",
  },
  {
    id: 92,
    first_name: "Agneta",
    last_name: "Belly",
    email: "abelly2j@unc.edu",
    gender: "Male",
  },
  {
    id: 93,
    first_name: "Rivi",
    last_name: "De Luna",
    email: "rdeluna2k@miibeian.gov.cn",
    gender: "Female",
  },
  {
    id: 94,
    first_name: "Price",
    last_name: "Luckhurst",
    email: "pluckhurst2l@last.fm",
    gender: "Female",
  },
  {
    id: 95,
    first_name: "Jedediah",
    last_name: "Seppey",
    email: "jseppey2m@barnesandnoble.com",
    gender: "Female",
  },
  {
    id: 96,
    first_name: "Audry",
    last_name: "Melrose",
    email: "amelrose2n@ehow.com",
    gender: "Female",
  },
  {
    id: 97,
    first_name: "Carie",
    last_name: "Hambribe",
    email: "chambribe2o@google.es",
    gender: "Female",
  },
  {
    id: 98,
    first_name: "Cam",
    last_name: "O'Kennedy",
    email: "cokennedy2p@craigslist.org",
    gender: "Female",
  },
];
